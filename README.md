# PaperRockScissors

A small and simple spring boot app to play Paper Rock Scissors.

First of all, please install 'docker'.

The command to run the app is
docker-compose up

The app's port is 8085
Then to access the app locally please use http://localhost:8085/

Home page is accessible either by / or /home in this page you can:
-Play a random round
-Play a round choosing ROCK as the selected form
-Restart the current game
-Go to statics Page

It also shows a rounds counter for the current game and a table with the form played for each player and the result.

The "Go to statics" button brings you to the statistics page.

Statics Page is accessible by /statics
if some rounds were played, it shows a chart with the statics to see the exact number of rounds won by each player please hover the mouse over each section, 
in the charts title you can see the total rounds played.

if there are no rounds in memory, it shows the legend "No rounds has played yet."

At the bottom there is a boton to go back to the home page