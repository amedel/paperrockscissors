package com.lottoland.challenge.statics;

import static com.lottoland.challenge.engine.DefaultGameEngine.DRAW;
import static com.lottoland.challenge.engine.DefaultGameEngine.MATCH_RESULT;
import static com.lottoland.challenge.engine.DefaultGameEngine.PLAYER_1;
import static com.lottoland.challenge.engine.DefaultGameEngine.PLAYER_2;
import static com.lottoland.challenge.statics.StaticsTranslator.translateFromRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lottoland.challenge.repository.GameRepository;
import com.lottoland.challenge.statics.model.StaticsModel;

@Component
public class DefaultStaticsExtractor implements StaticsExtractor {

	public static final String PLAYER_1_TOTAL = "totalPlayer1";
	public static final String PLAYER_2_TOTAL = "totalPlayer2";
	public static final String DRAW_TOTAL = "totalDraw";
	public static final String TOTAL_MATCHES = "totalRounds";
	
	@Autowired
	GameRepository gameRepository;
	
	@Override
	public StaticsModel getStatics(String UUID) {
		List<Map<String, String>> matchList = gameRepository.getMatchsByUUID(UUID);
		return translateFromRepository(UUID, matchList);
	}

	@Override
	public Map<String, Long> getStatics() {
		final Map<String, Long> mapStatics = new HashMap<>(4);
		final List<Map<String, String>> allMatchs = gameRepository.getAllMatchs();
		long player1WonMatches = allMatchs.stream().filter(match -> match.get(MATCH_RESULT).equals(PLAYER_1)).count();
		long player2WonMatches = allMatchs.stream().filter(match -> match.get(MATCH_RESULT).equals(PLAYER_2)).count();
		long drawMatches = allMatchs.stream().filter(match -> match.get(MATCH_RESULT).equals(DRAW)).count();
		long totalMatches = allMatchs.size();
		mapStatics.put(PLAYER_1_TOTAL, player1WonMatches);
		mapStatics.put(PLAYER_2_TOTAL, player2WonMatches);
		mapStatics.put(DRAW_TOTAL, drawMatches);
		mapStatics.put(TOTAL_MATCHES, totalMatches);
		return mapStatics;
	}

}
