package com.lottoland.challenge.statics;

import java.util.List;
import java.util.Map;

import com.lottoland.challenge.statics.model.StaticsModel;

public class StaticsTranslator {

	public static StaticsModel translateFromRepository(String UUID, List<Map<String, String>> matchList) {
		return new StaticsModel.Builder().setUserUUID(UUID).setMatchList(matchList).setPlayedRounds(matchList.size()).build();
		
	}

}
