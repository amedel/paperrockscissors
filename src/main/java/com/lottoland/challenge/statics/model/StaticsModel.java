package com.lottoland.challenge.statics.model;

import java.util.List;
import java.util.Map;

public class StaticsModel {

	private int playedRounds;
	private String userUUID;
	private List<Map<String, String>> matchList;
	
	private StaticsModel(int playedRounds, String userUUID, List<Map<String, String>> matchList) {
		this.playedRounds = playedRounds;
		this.userUUID = userUUID;
		this.matchList = matchList;
	}
	
	public int getPlayedRounds() {
		return playedRounds;
	}

	public String getUserUUID() {
		return userUUID;
	}

	public List<Map<String, String>> getMatchList() {
		return matchList;
	}

	public static class Builder{
		private int playedRounds;
		private String userUUID;
		private List<Map<String, String>> matchList;
		
		public Builder setPlayedRounds(int playedRounds) {
			this.playedRounds = playedRounds;
			return this;
		}
		public Builder setUserUUID(String userUUID) {
			this.userUUID = userUUID;
			return this;
		}
		public Builder setMatchList(List<Map<String, String>> matchList) {
			this.matchList = matchList;
			return this;
		}

		public StaticsModel build() {
			return new StaticsModel(playedRounds, userUUID, matchList);
		}
		
	}
	
}
