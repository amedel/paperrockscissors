package com.lottoland.challenge.statics;

import java.util.Map;

import com.lottoland.challenge.statics.model.StaticsModel;

public interface StaticsExtractor {

	StaticsModel getStatics(String UUID);

	Map<String, Long> getStatics();

}
