package com.lottoland.challenge.controller;

import java.util.Collections;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.lottoland.challenge.engine.GameEngine;
import com.lottoland.challenge.forms.FormEnum;
import com.lottoland.challenge.statics.StaticsExtractor;
import com.lottoland.challenge.statics.model.StaticsModel;

@Controller
public class GameController {

	public static final String COOKIE_NAME = "ucook";
	public static final String COOKIE_DEFAULT_VALUE = "N/A";

	@Autowired
	GameEngine gameEngine;
	
	@Autowired
	StaticsExtractor staticsExtractor;
	
	
	@GetMapping({"/","/home"})
	public ModelAndView home(@CookieValue(name = COOKIE_NAME, defaultValue = COOKIE_DEFAULT_VALUE ) String uCookie, HttpServletResponse response) {

		ModelAndView modelAndView = new ModelAndView("home");

		StaticsModel staticsModel;
		Cookie cookie; 
		if(!uCookie.equals(COOKIE_DEFAULT_VALUE)) {
			staticsModel = staticsExtractor.getStatics(uCookie);
			cookie = getCookie(uCookie);
		}else {
			staticsModel = new StaticsModel.Builder().setPlayedRounds(0).setMatchList(Collections.emptyList()).build();
			cookie = getCookie(UUID.randomUUID().toString());
		}
		modelAndView.addObject("statics", staticsModel);
		cookie.setHttpOnly(true);
		response.addCookie(cookie);

		return modelAndView;
	}
	
	@GetMapping({"/statics"})
	public ModelAndView statics() {
		ModelAndView modelAndView = new ModelAndView("statics");
		modelAndView.addAllObjects(staticsExtractor.getStatics());
		
		return modelAndView;
	}

	@PostMapping("/randomplay")
	public RedirectView randomPlay(@CookieValue(name = COOKIE_NAME, defaultValue = COOKIE_DEFAULT_VALUE ) String uCookie, HttpServletResponse response) {

		if(!uCookie.equals(COOKIE_DEFAULT_VALUE)) {
			gameEngine.autoPlayAndSave(uCookie);
		}
		return new RedirectView("home");
	}

	@PostMapping("/playrock")
	public RedirectView  playRock(@CookieValue(name = COOKIE_NAME, defaultValue = COOKIE_DEFAULT_VALUE ) String uCookie, HttpServletResponse response) {

		if(!uCookie.equals(COOKIE_DEFAULT_VALUE)) {
			gameEngine.playAndSave(uCookie, FormEnum.ROCK);
		}

		return new RedirectView("home");
	}

	@PostMapping("/restartgame")
	public RedirectView restartgame(@CookieValue(name = COOKIE_NAME, defaultValue = COOKIE_DEFAULT_VALUE ) Cookie uCookie, HttpServletResponse response) {

		if(!uCookie.getValue().equals(COOKIE_DEFAULT_VALUE)) {
			uCookie.setMaxAge(0);
			response.addCookie(uCookie);
		}

		return new RedirectView("home");
	}
	
	private Cookie getCookie(String UUID) {
		Cookie userCookie = new Cookie(COOKIE_NAME, UUID);
		userCookie.setHttpOnly(true);
		return userCookie;
	}
	
}
