package com.lottoland.challenge.engine;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.lottoland.challenge.forms.FormEnum;
import com.lottoland.challenge.repository.GameRepository;

@Component
public class DefaultGameEngine implements GameEngine {

	public static final String PLAYER_1 = "player1";
	public static final String PLAYER_2 = "player2";
	public static final String DRAW = "draw";
	public static final String MATCH_RESULT = "result";
	
	@Autowired
	GameRepository gameRepository;
	
	public FormEnum getRandomForm() {
		return new Random().ints(0, 3).mapToObj(i->FormEnum.values()[i]).findFirst().get();
	}

	public Map<String, String> autoPlay() {
		FormEnum player1 = getRandomForm();
		FormEnum player2 = getRandomForm();
		return play(player1,player2);
	}

	@Override
	public void autoPlayAndSave(String UUID) {
		Map<String, String> match = autoPlay();
		gameRepository.saveMatch(UUID, match);		
	}
	
	@Override
	public void playAndSave(String UUID, FormEnum userSelection) {
		FormEnum player2 = getRandomForm();
		Map<String, String> match = play(userSelection,player2);
		gameRepository.saveMatch(UUID, match);
	}
	
	public Map<String, String> play(FormEnum player1, FormEnum player2) {
		String result ;
		if(player1==player2) {
			result = DRAW;
		}else {
			result = player1.isStrongerThan(player2)?PLAYER_1:PLAYER_2;			
		}
		Map<String, String> map  = new HashMap<String, String>();
		map.put(PLAYER_1, player1.name());
		map.put(PLAYER_2, player2.name());
		map.put(MATCH_RESULT, result);
		return map;
	}

}
