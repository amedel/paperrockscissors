package com.lottoland.challenge.engine;

import com.lottoland.challenge.forms.FormEnum;

public interface GameEngine {

	public void autoPlayAndSave(String UUID);
	public void playAndSave(String UUID, FormEnum userSelection);
	
}
