package com.lottoland.challenge.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

@Repository
public class DefaultGameRepository implements GameRepository {

	private static final Map<String, List<Map<String, String>>> matchsMap = new ConcurrentHashMap<>();

	@Override
	public void saveMatch(String userUUID, Map<String, String> match) {
		Optional<List<Map<String, String>>> optionalMatchList = Optional.ofNullable(matchsMap.get(userUUID));
		if (optionalMatchList.isPresent()) {
			optionalMatchList.get().add(match);
		} else {
			matchsMap.put(userUUID, new ArrayList<>(Arrays.asList(match)));
		}
	}

	@Override
	public List<Map<String, String>> getMatchsByUUID(String UUID) {
		return Optional.ofNullable(matchsMap.get(UUID)).orElse(Collections.emptyList());
	}

	@Override
	public List<Map<String, String>> getAllMatchs() {
		return matchsMap.entrySet()
				        .stream()
				        .map(entry -> entry.getValue())
				        .flatMap(list -> list.stream())
				        .collect(Collectors.toList());
	}
	
	public Map<String, List<Map<String, String>>> getMatchsMapForTest(){
		return Collections.unmodifiableMap(matchsMap);
	}

	public void clearMap() {
		matchsMap.clear();
	}
	
}
