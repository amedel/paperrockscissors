package com.lottoland.challenge.repository;

import java.util.List;
import java.util.Map;

public interface GameRepository {

	public void saveMatch(String userUUID, Map<String, String> match);

	public List<Map<String, String>> getMatchsByUUID(String uUID);

	public List<Map<String, String>> getAllMatchs();
	
}
