package com.lottoland.challenge.forms;

public enum FormEnum {
	SCISSORS(0,1),
	PAPER(1,2),
	ROCK(2,0);

	private int ordinal;
	private int strongerThan;
	
	private FormEnum(int ordinal, int strongerThan) {
		this.ordinal = ordinal;
		this.strongerThan = strongerThan;
	}
	
	public boolean isStrongerThan(FormEnum anotherForm) {
		return this.strongerThan==anotherForm.getOrdinal();
	}

	public int getOrdinal() {
		return ordinal;
	}

	public int getStrongerThan() {
		return strongerThan;
	}
	
	
}
