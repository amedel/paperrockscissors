package com.lottoland.challenge.engine;


import static com.lottoland.challenge.engine.DefaultGameEngine.MATCH_RESULT;
import static com.lottoland.challenge.engine.DefaultGameEngine.PLAYER_2;
import static com.lottoland.challenge.forms.FormEnum.PAPER;
import static com.lottoland.challenge.forms.FormEnum.SCISSORS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lottoland.challenge.forms.FormEnum;
import com.lottoland.challenge.repository.GameRepository;

@SpringBootTest()
@RunWith(SpringRunner.class)
public class DefaultGameEngineTest {
	
	@Mock
	GameRepository gameRepository;
	
	@InjectMocks
	DefaultGameEngine gameEngine;

	@Test
	public void randomSelectionTest() {
		FormEnum form = gameEngine.getRandomForm();
		assertNotNull(form);
	}

	@Test
	public void playTest() {
		Map<String, String> playResultMap = gameEngine.play(PAPER, SCISSORS);
		assertNotNull(playResultMap);
		assertEquals(playResultMap.get(MATCH_RESULT), PLAYER_2);
	}
	
	@Test
	public void playAndSaveTest() {
		doNothing().when(gameRepository).saveMatch(eq("1"), any());
		gameEngine.playAndSave("1", SCISSORS);
		verify(gameRepository, atLeastOnce()).saveMatch(eq("1") , any());
	}

	
}
