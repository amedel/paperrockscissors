package com.lottoland.challenge.forms;

import static com.lottoland.challenge.forms.FormEnum.PAPER;
import static com.lottoland.challenge.forms.FormEnum.ROCK;
import static com.lottoland.challenge.forms.FormEnum.SCISSORS;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class FormEnumTest {

	@Test
	public void StrogernessTest() {
		assertTrue(SCISSORS.isStrongerThan(PAPER));
		assertTrue(ROCK.isStrongerThan(SCISSORS));
		assertTrue(PAPER.isStrongerThan(ROCK));
	}
	
	
}
