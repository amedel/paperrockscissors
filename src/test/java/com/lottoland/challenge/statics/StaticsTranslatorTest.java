package com.lottoland.challenge.statics;

import static com.lottoland.challenge.engine.DefaultGameEngine.MATCH_RESULT;
import static com.lottoland.challenge.engine.DefaultGameEngine.PLAYER_1;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.lottoland.challenge.statics.model.StaticsModel;

public class StaticsTranslatorTest {

	@Test
	public void translateFromRepositoryTest() {
		HashMap<String, String> hashMap = new HashMap<>();
		hashMap.put(MATCH_RESULT, PLAYER_1);
		
		List<Map<String, String>> mapList = new ArrayList<>(Arrays.asList(hashMap));
		StaticsModel staticsModel = StaticsTranslator.translateFromRepository("1", mapList);
				
		assertEquals("1", staticsModel.getUserUUID());
		assertTrue(staticsModel.getMatchList().stream()
				                              .filter(match -> match.get(MATCH_RESULT).equals(PLAYER_1))
			                             	  .findFirst()
			                             	  .isPresent());
	}
	
}
