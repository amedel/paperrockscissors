package com.lottoland.challenge.statics;

import static com.lottoland.challenge.engine.DefaultGameEngine.DRAW;
import static com.lottoland.challenge.engine.DefaultGameEngine.MATCH_RESULT;
import static com.lottoland.challenge.engine.DefaultGameEngine.PLAYER_1;
import static com.lottoland.challenge.engine.DefaultGameEngine.PLAYER_2;
import static com.lottoland.challenge.forms.FormEnum.PAPER;
import static com.lottoland.challenge.forms.FormEnum.ROCK;
import static com.lottoland.challenge.forms.FormEnum.SCISSORS;
import static com.lottoland.challenge.statics.DefaultStaticsExtractor.DRAW_TOTAL;
import static com.lottoland.challenge.statics.DefaultStaticsExtractor.PLAYER_1_TOTAL;
import static com.lottoland.challenge.statics.DefaultStaticsExtractor.PLAYER_2_TOTAL;
import static com.lottoland.challenge.statics.DefaultStaticsExtractor.TOTAL_MATCHES;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lottoland.challenge.repository.GameRepository;

@SpringBootTest()
@RunWith(SpringRunner.class)
public class DefaultStaticsExtractorTest {

	@Mock
	GameRepository gameRepository;
	
	@InjectMocks
	DefaultStaticsExtractor defaultStaticsExtractor;
	
	@Test
	public void getStaticsTest() {
		HashMap<String, String> match1 = new HashMap<>();
		match1.put(MATCH_RESULT, PLAYER_1);
		match1.put(PLAYER_1, ROCK.name());
		match1.put(PLAYER_2, SCISSORS.name());
		HashMap<String, String> match2 = new HashMap<>();
		match2.put(MATCH_RESULT, PLAYER_1);
		match2.put(PLAYER_1, ROCK.name());
		match2.put(PLAYER_2, SCISSORS.name());
		HashMap<String, String> match3 = new HashMap<>();
		match3.put(MATCH_RESULT, PLAYER_1);
		match3.put(PLAYER_1, ROCK.name());
		match3.put(PLAYER_2, SCISSORS.name());
		HashMap<String, String> match4 = new HashMap<>();
		match4.put(MATCH_RESULT, PLAYER_2);
		match4.put(PLAYER_1, PAPER.name());
		match4.put(PLAYER_2, SCISSORS.name());
		HashMap<String, String> match5 = new HashMap<>();
		match5.put(MATCH_RESULT, PLAYER_2);
		match5.put(PLAYER_1, PAPER.name());
		match5.put(PLAYER_2, SCISSORS.name());
		HashMap<String, String> match6 = new HashMap<>();
		match6.put(MATCH_RESULT, DRAW);
		match6.put(PLAYER_1, PAPER.name());
		match6.put(PLAYER_2, PAPER.name());
		
		List<Map<String,String>> listMatchs = Arrays.asList(match1,match2,match3,match4,match5,match6);
		
		when(gameRepository.getAllMatchs()).thenReturn(listMatchs);
		Map<String, Long> staticsMap = defaultStaticsExtractor.getStatics();
		assertEquals(3L, staticsMap.get(PLAYER_1_TOTAL).longValue());
		assertEquals(2L, staticsMap.get(PLAYER_2_TOTAL).longValue());
		assertEquals(1L, staticsMap.get(DRAW_TOTAL).longValue());
		assertEquals(6L, staticsMap.get(TOTAL_MATCHES).longValue());
		
	}
}
