package com.lottoland.challenge.repository;

import static com.lottoland.challenge.engine.DefaultGameEngine.MATCH_RESULT;
import static com.lottoland.challenge.engine.DefaultGameEngine.PLAYER_1;
import static com.lottoland.challenge.engine.DefaultGameEngine.PLAYER_2;
import static com.lottoland.challenge.forms.FormEnum.ROCK;
import static com.lottoland.challenge.forms.FormEnum.SCISSORS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class DefaultGameRepositoryTest {

	DefaultGameRepository gameRepository =  new DefaultGameRepository();
	HashMap<String, String> match;
	
	@Before
	public void setup() {
		match = new HashMap<>();
		match.put(MATCH_RESULT, PLAYER_1);
		match.put(PLAYER_1, ROCK.name());
		match.put(PLAYER_2, SCISSORS.name());
	}
	
	@Test
	public void getMatchsByUUIDOnEmptyMap() {
		gameRepository.clearMap();
		List<Map<String, String>> list = gameRepository.getMatchsByUUID("1");
		assertNotNull(list);
		assertTrue(list.isEmpty());
	}

	@Test
	public void getAllMatchsOnEmptyMap() {
		gameRepository.clearMap();
		List<Map<String, String>> list = gameRepository.getAllMatchs();
		assertNotNull(list);
		assertTrue(list.isEmpty());
	}
	
	@Test
	public void saveMatchUUDINotExistTest() {
		gameRepository.saveMatch("2", match);
		assertEquals(1,gameRepository.getMatchsMapForTest().entrySet().size());
	}
	
	@Test
	public void saveMatchWithExistentUUDITest() {
		gameRepository.saveMatch("3", match);
		gameRepository.saveMatch("3", match);
		assertEquals(2,gameRepository.getMatchsMapForTest().get("3").size());
	}

	@Test
	public void getMatchsByUUID() {
		gameRepository.saveMatch("4", match);
		List<Map<String, String>> list = gameRepository.getMatchsByUUID("4");
		assertNotNull(list);
		assertFalse(list.isEmpty());
	}
	
	@Test
	public void getAllMatchsByUUID() {
		gameRepository.saveMatch("5", match);
		gameRepository.saveMatch("6", match);
		gameRepository.saveMatch("5", match);
		gameRepository.saveMatch("7", match);
		List<Map<String, String>> list = gameRepository.getAllMatchs();
		assertNotNull(list);
		assertFalse(list.isEmpty());
		assertTrue(list.size()>=4);
	}
	
}
